## Alternative compile option

For compiling you normally need the `eql5` executable; there may be reasons you
don't want or can't use it.

So, if you want to compile `eql5` code using vanilla `ecl`, you just need to
load `EQL5-symbols.lisp` first, and it will pretend to be `eql5`!

(See also note in `EQL5-symbols.lisp` file.)

