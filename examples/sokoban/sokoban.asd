(defsystem :sokoban
  :serial t
  :depends-on ()
  :components ((:file "lisp/qml-lisp")
               (:file "lisp/package")
               (:file "lisp/ui-vars")
               (:file "lisp/3rd-party/sokoban")
               (:file "lisp/3rd-party/my-levels")
               (:file "lisp/sokoban")))

