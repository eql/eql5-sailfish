#include <ecl/ecl.h>
#include <eql5/eql.h>
#include <eql5/eql_fun.h>
#include <sailfishapp.h>
#include <QGuiApplication>
#include <QQuickView>
#include <QTextCodec>

extern "C" void ini_app(cl_object);

int main(int argc, char* argv[]) {
    SailfishApp::application(argc, argv);

    QTextCodec* utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);

    EQL eql;
    eql.exec(ini_app);
    eql_fun("qml:ini-sailfish",
            Q_ARG(QUrl, SailfishApp::pathToMainQml()),
            Q_ARG(QUrl, SailfishApp::pathTo("")),
            Q_ARG(QQuickView*, SailfishApp::createView()));

    return 0; }

