;;;
;;; This is a QML GUI for CL-Sokoban, see http://www.cliki.net/CL-Sokoban 
;;;
;;; Use CHANGE-LEVEL to directly change the level index.
;;;

(eql:qrequire :quick)

(defpackage :sokoban
  (:use :cl :eql :qml)
  (:export
   #:change-level
   #:run))

(in-package :sokoban)

(defvar *item-types*
  '((#\# . :wall)
    (#\$ . :object)
    (#\* . :object2)
    (#\. . :goal)
    (#\@ . :player)
    (#\& . :player2)))

(defvar *items*         nil)
(defvar *item-size*     nil)
(defvar *maze*          nil)
(defvar *my-mazes*      (mapcar 'game:copy-maze game:*mazes*))
(defvar *solving*       nil)
(defvar *undo-stack*    nil)
(defvar *level-changed* nil)

(defun qml-component (file)
  (qnew "QQmlComponent(QQmlEngine*,QUrl)"
        (|engine| qml:*quick-view*)
        (file-to-url (x:cc "qml/items/" file))))

(defvar *player-item*) ; :player
(defvar *box-item*)    ; :object
(defvar *box-item-2*)  ; :object2
(defvar *static-item*) ; :wall :goal

(defun board ()
  (find-quick-item ui:*board*))

(defun level ()
  (if (find-quick-item ui:*level*)
      (floor (q< |value| ui:*level*))
      0))

(defun set-level (index)
  (q> |value| ui:*level* index))

(defun assoc* (item alist)
  (cdr (assoc item alist)))

(defun char-type (char)
  (cdr (assoc char *item-types*)))

(defun type-char (type)
  (car (find type *item-types* :key 'cdr)))

(defun set-maze ()
  (setf *maze* (nth (level) *my-mazes*))
  (update-translate-xy)
  (create-items)
  (place-all-items)
  (setf *undo-stack* nil))

(defun reset-maze ()
  (setf *maze* (setf (nth (level) *my-mazes*)
                     (game:copy-maze (nth (level) game:*mazes*))))
  (update-placed-items t)
  (setf *undo-stack* nil))

(defvar *translate-x* 0)
(defvar *translate-y* 0)

(defun update-translate-xy ()
  "Set x and y translation for maze centering."
  (let ((dim (game:maze-dimensions *maze*))
        (img-px 32)
        (board-size 16))
    (setf *translate-x* (floor (/ (* img-px (- board-size (car dim))) 2))
          *translate-y* (floor (/ (* img-px (- board-size (cdr dim))) 2)))))
  
(defun create-item-type (type)
  (qt-object-? (|create| (case type
                           (:object *box-item*)
                           (:object2 *box-item-2*)
                           ((:player :player2) *player-item*)
                           ((:wall :goal) *static-item*)))))

(defun create-item (type)
  (let ((item (create-item-type type)))
    (q> |source| item (file-to-url (format nil "qml/img/~(~A~).png" type)))
    (|setObjectName| item (string-downcase type))
    (unless *item-size*
      (setf *item-size* (q< |sourceSize| item)))
    item))

(defun create-items ()
  (clear-items)
  (flet ((add (types)
           (dolist (type (x:ensure-list types))
             (let ((item (create-item type)))
               (push item (cdr (assoc type *items*)))
               (|setParent| item (board))
               (|setParentItem| item (board))))))
    (dolist (row (game:maze-text *maze*))
      (x:do-string (char row)
        (unless (char= #\Space char)
          (let ((type (char-type char)))
            (cond ((find type '(:player :player2))
                   (add '(:player :player2)))
                  ((find type '(:object :object2))
                   (add '(:object :object2 :goal)))
                  ((eql :wall type)
                   (add :wall)))))))))

(defvar *no-delete* nil)

(defun clear-items ()
  (unless *no-delete*
    (dolist (items *items*)
      (dolist (item (rest items))
        (qdel item))))
  (setf *items* (mapcar (lambda (x) (list (cdr x))) *item-types*)))

(defvar *running-animations* 0)
(defvar *function-queue*     nil)

(defun animation-change (running) ; called from QML
  (incf *running-animations* (if running 1 -1))
  (x:while (and (zerop *running-animations*)
                *function-queue*)
    (funcall (pop *function-queue*))))

(defun run-or-enqueue (function)
  (if (zerop *running-animations*)
      (funcall function)
      (setf *function-queue* (nconc *function-queue* (list function)))))

(defmacro queued (&rest functions)
  "Run passed functions in order, waiting for currently running (or newly triggered) animations to finish first."
  `(progn
     ,@(mapcar (lambda (fun) `(run-or-enqueue (lambda () ,fun)))
               functions)))

(defun change-level (direction/index)
  "Changes *LEVEL* in given direction or to index."
  (let ((level (min (1- (length *my-mazes*))
                    (max 0 (if (numberp direction/index)
                               direction/index
                               (+ (if (eql :next direction/index) 1 -1)
                                  (level)))))))
    (when (/= level (level))
      (queued (q> |running| ui:*zoom-board-out* t)
              (set-level level) ; will call SET-MAZE from QML
              (q> |running| ui:*zoom-board-in* t))))
  (setf *level-changed* t)
  (level))

(defun solve ()
  (setf *level-changed* nil)
  (let ((*solving* t))
    (reset-maze)
    (x:do-string (ch (nth (level) game:*solutions*))
      (when *level-changed*
        (setf *level-changed nil)
        (return-from solve))
      (game:move (case (char-downcase ch)
                   (#\u :north)
                   (#\d :south)
                   (#\l :west)
                   (#\r :east))
                 *maze*)
      (x:while (plusp *running-animations*)
        (qsleep 0.05)))))

(defun set-x (item x &optional animate)
  (let ((x* (+ x *translate-x*)))
    (if animate
        (qjs |setX| item x*) ; workaround (Sailfish only); was: '(q> |x| item x*)'
        (|setX| item x*))))

(defun set-y (item y &optional animate)
  (let ((y* (+ y *translate-y*)))
    (if animate
        (qjs |setY| item y*) ; workaround (Sailfish only); was: '(q> |y| item y*)'
        (|setY| item y*))))

(defun child-at (x y)
  (|childAt| (board) (+ x *translate-x*) (+ y *translate-y*)))

(defun place-items (type &optional reset)
  (let ((char (type-char type))
        (items (assoc* type *items*))
        (y 0))
    (unless (eql :wall type)
      (dolist (item items)
        (|setVisible| item nil)))
    (dolist (row (game:maze-text *maze*))
      (let ((x 0))
        (x:do-string (curr-char row)
          (when (char= char curr-char)
            (let ((item (first items)))
              (|setVisible| item t)
              (set-x item x)
              (set-y item y))
            (setf items (rest items)))
          (incf x (first *item-size*))))
      (incf y (second *item-size*)))))

(defun place-all-items ()
  (dolist (type '(:wall :goal :object2 :player2 :player :object))
    (place-items type)))

(defun update-placed-items (&optional reset)
  (dolist (type '(:goal :object2 :player2 :player :object))
    (place-items type reset)))

(defparameter *its* nil)

(let (ex ex-ex)
  (defun move-item (char pos direction) ; see game:*move-hook*
    (let* ((type (char-type char))
           (pos-x (car pos))
           (pos-y (cdr pos))
           (w (first *item-size*))
           (h (second *item-size*))
           (x (* w pos-x))
           (y (* h pos-y))
           (dx (case direction (:east w) (:west (- w)) (t 0)))
           (dy (case direction (:south h) (:north (- h)) (t 0)))
           (item (child-at (+ x (/ w 2)) (+ y (/ h 2)))))
      (pushnew item *its* :test 'qeql)
      (unless (qnull item)
        (if (zerop dy)
            (set-x item (+ x dx) 'animate)
            (set-y item (+ y dy) 'animate))
        (dolist (tp (list type ex ex-ex))
          (when (find tp '(:player2  :object2 :goal))
            (queued (update-placed-items))
            (return)))
        (shiftf ex-ex ex type)
        (when (eql :player type)
          (qlater (lambda () (when (game-finished)
                               (final-animation)))))))))

(defun add-undo-step (step)
  (push step *undo-stack*))

(defun undo ()
  (when *undo-stack*
    (game:undo *maze* (pop *undo-stack*))
    (update-placed-items)))

(defun game-finished ()
  ;; finished: no more :object, only :object2
  (let ((ch (type-char :object)))
    (dolist (str (game:maze-text *maze*))
      (when (find ch str)
        (return-from game-finished))))
  t)

(defun final-animation ()
  (queued (q> |running| ui:*rotate-player* t)
          (q>* |running| ui:*wiggle-box* t)))

(defun connect ()
  (flet ((pressed (item function)
           (qconnect (find-quick-item item) "pressed()" function)))
    (pressed ui:*up*       (lambda () (game:move :north *maze*)))
    (pressed ui:*down*     (lambda () (game:move :south *maze*)))
    (pressed ui:*left*     (lambda () (game:move :west *maze*)))
    (pressed ui:*right*    (lambda () (game:move :east *maze*)))
    (pressed ui:*previous* (lambda () (change-level :previous)))
    (pressed ui:*next*     (lambda () (change-level :next)))
    (pressed ui:*undo*     'undo)
    (pressed ui:*restart*  'reset-maze)
    (pressed ui:*solve*    (lambda () (qlater 'solve))))) ; QLATER: prevent timer problem

(defun run ()
  (setf *player-item* (qml-component "player.qml")
        *box-item*    (qml-component "box.qml")
        *box-item-2*  (qml-component "box2.qml")
        *static-item* (qml-component "static.qml"))
  (qconnect qml:*quick-view* "statusChanged(QQuickView::Status)" ; for reloading
            (lambda (status)
              (case status
                (#.|QQuickView.Ready|
                 (qml-reloaded)))))
  (connect)
  (setf game:*move-hook* 'move-item
        game:*undo-hook* 'add-undo-step)
  (q> |maximumValue| ui:*level* (1- (length *my-mazes*)))
  (set-maze))

(defun qml-reloaded ()
  (connect)
  (let ((*no-delete* t))
    (set-maze))
  (q> |maximumValue| ui:*level* (1- (length *my-mazes*))))

(qlater 'run) ; QLATER: wait until QQuickView is ready

