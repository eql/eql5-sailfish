;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*board*
   #:*buttons1*
   #:*buttons2*
   #:*down*
   #:*left*
   #:*level*
   #:*next*
   #:*previous*
   #:*restart*
   #:*right*
   #:*rotate-player*
   #:*solve*
   #:*undo*
   #:*up*
   #:*wiggle-box*
   #:*zoom-board-in*
   #:*zoom-board-out*))

(provide :ui-vars)

(in-package :ui)

(defparameter *down*           "down")           ; ArrowButton         "qml/sokoban.qml"
(defparameter *left*           "left")           ; ArrowButton         "qml/sokoban.qml"
(defparameter *right*          "right")          ; ArrowButton         "qml/sokoban.qml"
(defparameter *up*             "up")             ; ArrowButton         "qml/sokoban.qml"
(defparameter *next*           "next")           ; Button              "qml/sokoban.qml"
(defparameter *previous*       "previous")       ; Button              "qml/sokoban.qml"
(defparameter *restart*        "restart")        ; Button              "qml/sokoban.qml"
(defparameter *solve*          "solve")          ; Button              "qml/sokoban.qml"
(defparameter *undo*           "undo")           ; Button              "qml/sokoban.qml"
(defparameter *board*          "board")          ; Rectangle           "qml/sokoban.qml"
(defparameter *rotate-player*  "rotate_player")  ; RotationAnimation   "qml/items/player.qml"
(defparameter *buttons1*       "buttons1")       ; Row                 "qml/sokoban.qml"
(defparameter *buttons2*       "buttons2")       ; Row                 "qml/sokoban.qml"
(defparameter *zoom-board-in*  "zoom_board_in")  ; ScaleAnimator       "qml/sokoban.qml"
(defparameter *zoom-board-out* "zoom_board_out") ; ScaleAnimator       "qml/sokoban.qml"
(defparameter *wiggle-box*     "wiggle_box")     ; SequentialAnimation "qml/items/box2.qml"
(defparameter *level*          "level")          ; Slider              "qml/sokoban.qml"

