# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

LISP_FILES = make.lisp \
  lisp/3rd-party/levels.lisp \
  lisp/3rd-party/my-levels.lisp \
  lisp/3rd-party/sokoban.lisp \
  lisp/dependencies.lisp \
  lisp/sokoban.lisp \
  lisp/ui-vars.lisp \
  app.asd

lisp.output = libapp.a
# 2 variants to do the same thing
lisp.commands = eql5 -platform minimal $$PWD/make.lisp # variant 1
#lisp.commands = ecl -shell $$PWD/make.lisp            # variant 2
lisp.input = LISP_FILES

QMAKE_EXTRA_COMPILERS += lisp

CONFIG += no_keywords sailfishapp # sailfishapp_i18n
LIBS += -L. -lapp -lecl -leql5
TARGET = sokoban

SOURCES += src/main.cpp

DISTFILES += qml/sokoban.qml \
  qml/ext/Button.qml \
  qml/ext/ArrowButton.qml \
  qml/ext/NumberAnimation.qml \
  qml/ext/RotationAnimation.qml \
  qml/ext/ScaleAnimator.qml \
  qml/ext/SequentialAnimation.qml \
  qml/items/box2.qml \
  qml/items/box.qml \
  qml/items/player.qml \
  qml/items/static.qml \
  qml/fonts/fontawesome-webfont.ttf \
  qml/img/goal.png \
  qml/img/object2.png \
  qml/img/object.png \
  qml/img/player2.png \
  qml/img/player.png \
  qml/img/wall.png \
  #translations/*.qm \
  \ # Sailfish specific
  qml/cover/CoverPage.qml \
  rpm/sokoban.changes.in \
  rpm/sokoban.changes.run.in \
  rpm/sokoban.spec \
  rpm/sokoban.yaml \
  sokoban.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

#TRANSLATIONS += translations/sokoban-xx.ts
