## Note

For the original template see [eql5-sfos](https://redmine.casenave.fr/projects/eql5-sfos/repository)


## Porting experience

Here a few things I found while 'porting' this example from android:

- pixel ratio is different; I needed to multiply pixel sizes by 3
- can't access e.g. `x`, `y`, `width`, `height` through QML properties using
`q<` and `q>`, must use e.g. `|width|`, `|setWidth|` instead; this affects
attached animations, because they are not triggered using direct Qt function
calls; a workaround is to use JS functions, like `function setX(_x) { x = _x }`
and call them from Lisp: `(qjs |setX| item x)`
- Silica QML classes have nice, mobile friendly and consistently beautiful
design, but are different from modern day QML classes; it's generally easy
enough though to extend them to one's personal needs


## Compile note

If you encounter problems when compiling the Lisp part, please see
`../../utils/README.md`.

