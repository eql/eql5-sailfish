import QtQuick 2.6
import EQL5 1.0

SequentialAnimation {
  onRunningChanged: Lisp.call("sokoban:animation-change", running)
}
