import QtQuick 2.6
import Sailfish.Silica 1.0

Button {
  width: 105
  height: width

  signal pressed()

  MouseArea {
    anchors.fill: parent
    onPressed: parent.pressed()
  }
}
