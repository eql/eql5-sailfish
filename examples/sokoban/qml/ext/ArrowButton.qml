import QtQuick 2.6
import Sailfish.Silica 1.0

Label {
  width: 114
  height: width
  font.pixelSize: 114
  opacity: 0.2
  scale: 1.2

  signal pressed()

  MouseArea {
    anchors.fill: parent
    onPressed: parent.pressed()
  }
}
