import QtQuick 2.6
import Sailfish.Silica 1.0
import "./ext/" as Ext
import EQL5 1.0

ApplicationWindow {
  id: main

  FontLoader {
    id: fontAwesome
    source: "fonts/fontawesome-webfont.ttf"
  }

  initialPage: Component {
    Page {
      Slider {
        id: level
        objectName: "level"
        y: 50
        width: Screen.width
        stepSize: 1.0

        onValueChanged: Lisp.call("sokoban:set-maze")
      }

      Column {
        // put 'board' into a container, so we don't lose 'scale' after changing the level
        anchors.centerIn: parent
        scale: Screen.width / width

        Rectangle {
          id: board
          objectName: "board"
          width: 512
          height: 512
          color: "lightsteelblue"
        }
      }

      Row {
        id: buttons1
        objectName: "buttons1"
        spacing: 30
        padding: 30
        anchors.bottom: parent.bottom

        Ext.Button {
          objectName: "previous"
          text: "\uf100"
        }
        Ext.Button {
          objectName: "next"
          text: "\uf101"
        }
      }

      Row {
        id: buttons2
        objectName: "buttons2"
        spacing: 30
        padding: 30
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Ext.Button {
          objectName: "undo"
          text: "\uf112"
        }
        Ext.Button {
          objectName: "restart"
          text: "\uf0e2"
        }
        Ext.Button {
          objectName: "solve"
          text: "\uf17b"
        }
      }

      // container for arrow buttons

      Item {
        id: arrows
        y: buttons1.y - height - 75
        width: 3 * up.width
        height: 3 * up.height
        anchors.rightMargin: 33
        anchors.right: parent.right

        Ext.ArrowButton {
          id: up
          objectName: "up"
          text: "\uf139"
          anchors.horizontalCenter: parent.horizontalCenter
        }

        Ext.ArrowButton {
          objectName: "left"
          text: "\uf137"
          anchors.verticalCenter: parent.verticalCenter
        }

        Ext.ArrowButton {
          objectName: "right"
          text: "\uf138"
          anchors.verticalCenter: parent.verticalCenter
          anchors.right: parent.right
        }

        Ext.ArrowButton {
          objectName: "down"
          text: "\uf13a"
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.bottom: parent.bottom
        }
      }

      // level change animations

      Ext.ScaleAnimator {
      objectName: "zoom_board_out"
        target: board
        from: 1.0
        to: 0.0
        duration: 250
      }

      Ext.ScaleAnimator {
      objectName: "zoom_board_in"
        target: board
        from: 0.0
        to: 1.0
        duration: 250
      }
    }
  }

  // corresponding function to 'qml:message'

  function message(text, header) {
    var dialog = pageStack.push(Qt.resolvedUrl("ext/Message.qml"),
                                { text: text, header: header })
    dialog.accepted.connect(function() { Lisp.call("qml:%done", true) })
    dialog.rejected.connect(function() { Lisp.call("qml:%done", false) })
  }
}
