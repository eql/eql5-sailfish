import QtQuick 2.0
import "../ext/" as Ext

Image {
  Behavior on x {
    Ext.NumberAnimation {
      duration: 150
      easing.type: Easing.InQuart
    }
  }

  Behavior on y {
    Ext.NumberAnimation {
      duration: 150
      easing.type: Easing.InQuart
    }
  }
  
  // workaround (Sailfish only)
  function setX(_x) { x = _x }
  function setY(_y) { y = _y }
}
