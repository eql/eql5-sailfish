#-eql
(load "../../utils/EQL5-symbols")

(require :asdf)

(load "lisp/dependencies")

(push *default-pathname-defaults* asdf:*central-registry*)

(asdf:make-build "app"
                 :monolithic t
                 :type :static-library
                 :move-here "./"
		 :init-name "ini_app")

(let ((lib-name "libapp.a"))
  (when (probe-file lib-name)
    (delete-file lib-name))
  (rename-file (x:cc "app--all-systems" ".a") lib-name))

#+eql
(eql:qquit)
