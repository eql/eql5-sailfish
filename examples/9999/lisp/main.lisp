(in-package :eql-user)

(use-package :qml)

(defvar *number* 0)
(defvar *canvas* "canvas")
(defvar *input*  "input")

(defun draw-line (x1 y1 x2 y2)
  (qjs |drawLine| *canvas*
       x1 y1 x2 y2))

(defun draw-number (number)
  (x:when-it (find-quick-item *canvas*) ; NIL on startup
    (setf *number* number)
    (q! |requestPaint| x:it)))

(defun paint () ; called from QML
  (draw-line 0 -450 0 450)
  (let ((dy -150)
        (dig 1))
    (labels ((line (x1 y1 x2 y2)
               (when (find dig '(2 4))
                 (setf x1 (- x1)
                       x2 (- x2)))
               (when (>= dig 3)
                 (setf y1 (- y1)
                       y2 (- y2)
                       dy 150))
               (draw-line (* 300 x1) (+ dy (* 300 y1))
                          (* 300 x2) (+ dy (* 300 y2))))
             (draw (n)
               (case n
                 (1 (line 0 -1  1 -1))
                 (2 (line 0  0  1  0))
                 (3 (line 0 -1  1  0))
                 (4 (line 0  0  1 -1))
                 (5 (draw 1) (draw 4))
                 (6 (line 1 -1  1  0))
                 (7 (draw 1) (draw 6))
                 (8 (draw 2) (draw 6))
                 (9 (draw 1) (draw 8)))))
      (let ((num *number*))
        (x:while (plusp num)
          (draw (mod num 10))
          (setf num (floor (/ num 10)))
          (incf dig))))))

(qlater (lambda () (q> |focus| *input* t)))
