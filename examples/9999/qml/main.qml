import QtQuick 2.6
import Sailfish.Silica 1.0
import EQL5 1.0

ApplicationWindow {

  TextField {
    id: input
    objectName: "input"
    width: parent.width
    horizontalAlignment: Qt.AlignHCenter
    text: "0000"
    validator: IntValidator { bottom: 0; top: 9999 }
    inputMethodHints: Qt.ImhDigitsOnly

    onTextChanged: Lisp.call("eql-user:draw-number", Number(text))
  }
  
  Canvas {
    id: canvas
    objectName: "canvas"
    anchors.centerIn: parent
    width: 660
    height: 960

    property var painter
    
    function drawLine(x1, y1, x2, y2) {
      painter.moveTo(x1, y1)
      painter.lineTo(x2, y2)
    }

    onPaint: {
      var ctx = getContext("2d")
      painter = ctx
      ctx.reset()
      ctx.strokeStyle = Theme.highlightColor
      ctx.lineWidth = 30
      ctx.lineCap = "round"
      ctx.translate(width / 2, height / 2)
      
      Lisp.call("eql-user:paint")
      
      ctx.stroke()
    }
  }
}
