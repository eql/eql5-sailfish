// for (c) please see COPYING.txt

import QtQuick 2.6
import Sailfish.Silica 1.0
import "ext/" as Ext
import EQL5 1.0

ApplicationWindow {
  id: game
  objectName: "game"

  property bool running: true
  property int difficulty: 10 // chance it will actually think

  Image {
    id: boardImage
    objectName: "board_image"
    anchors.centerIn: parent
    source: "../pics/board.png"
    scale: Screen.width / 400
  }

  Column {
    id: display
    objectName: "display"
    anchors.centerIn: parent
    scale: boardImage.scale

    Grid {
      id: board
      objectName: "board"
      width: boardImage.width
      height: boardImage.height
      columns: 3

      Repeater {
        model: 9

        Ext.TicTac {
          width: board.width / 3
          height: board.height / 3

          onClicked: Lisp.call("logic:tic-tac-clicked", index)
        }
      }
    }
  }

  Row {
    spacing: 10
    bottomPadding: 10
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.bottom

    Button {
      id: button
      width: Screen.width / 3 - parent.spacing
      text: "Hard"
      down: game.difficulty == 10
      onClicked: { game.difficulty = 10 }
    }
    Button {
      width: button.width
      text: "Moderate"
      down: game.difficulty == 8
      onClicked: { game.difficulty = 8 }
    }
    Button {
      width: button.width
      text: "Easy"
      down: game.difficulty == 2
      onClicked: { game.difficulty = 2 }
    }
  }

  Text {
    id: messageDisplay
    objectName: "message_display"
    anchors.centerIn: parent
    font.pixelSize: Screen.width / 4
    font.bold: true
    color: "blue"
    style: Text.Outline; styleColor: "white"
    visible: false

    Timer {
      running: messageDisplay.visible
      onTriggered: Lisp.call("logic:restart-game")
    }
  }
}
