## Android APK note

Some example like the CL REPL (see android version) will not be ported to
Sailfish, because it works perfectly well in the android layer of Sailfish
(which of course requires you to purchase a **Sailfish X** license from Jolla).

You can get APKs for CL REPL (and for QML Creator) from here:

[cl-repl.org](http://cl-repl.org)
