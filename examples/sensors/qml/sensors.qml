import QtQuick 2.6
import Sailfish.Silica 1.0
import QtSensors 5.6
import QtPositioning 5.2
import EQL5 1.0

ApplicationWindow {
  id: main
  objectName: "main"
  
  initialPage: Component {
    Page {

      Text {
        padding: 50
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeMedium
        text: "<br><b>Available sensors:</b><br><ul><li>" + QmlSensors.sensorTypes().sort().join("<li>") + "</ul>"
      }

      Image {
        objectName: "bubble"
        source: "img/bubble.svg"
        smooth: true
        x: (main.width - width) / 2
        y: (main.height - width) / 2
        z: 1
      }

      Text {
        objectName: "location"
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        bottomPadding: 150
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeMedium
      }

      // accelerometer

      Accelerometer {
        id: accel
        active: true
      }

      Timer {
        interval: 5
        running: true
        repeat: true
        onTriggered: Lisp.call("sensors:move-bubble", accel.reading.x, accel.reading.y, accel.reading.z)
      }

      // GPS

      Component {
        id: gps

        PositionSource {
          updateInterval: 1000
          active: true

          onPositionChanged: {
            Lisp.call("sensors:position-changed",
                  position.latitudeValid           ? position.coordinate.latitude  : null,
                  position.longitudeValid          ? position.coordinate.longitude : null,
                  position.altitudeValid           ? position.coordinate.altitude  : null,
                  position.horizontalAccuracyValid ? position.horizontalAccuracy   : null,
                  position.verticalAccuracyValid   ? position.verticalAccuracy     : null,
                  position.speedValid              ? position.speed                : null,
                  position.directionValid          ? position.direction            : null,
                  position.magneticVariationValid  ? position.magneticVariation    : null,
                  position.timestamp.toLocaleString(Qt.locale(), "yyyy-MM-dd hh:mm:ss"))
          }
        }
      }

      Loader {
        objectName: "gps_loader"
        active: false
        sourceComponent: gps
      }
    }
  }
}
