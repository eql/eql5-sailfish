;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*bubble*
   #:*gps-loader*
   #:*location*
   #:*main*))

(provide :ui-vars)

(in-package :ui)

(defparameter *main*       "main")       ; ApplicationWindow "qml/sensors.qml"
(defparameter *gps-loader* "gps_loader") ; Loader            "qml/sensors.qml"
(defparameter *bubble*     "bubble")     ; Image             "qml/sensors.qml"
(defparameter *location*   "location")   ; Text              "qml/sensors.qml"
