;;; This is an extended port of QML example "accelbubble"

(eql:qrequire :quick)

(in-package :sensors)

(defvar *max-x* nil)
(defvar *max-y* nil)

(si::trap-fpe t nil) ; ignore floating point overflows

(defun ini ()
  (let ((main (find-quick-item ui:*main*))
        (bubble (find-quick-item ui:*bubble*)))
    (setf *max-x* (- (|width| main) (|width| bubble))
          *max-y* (- (|height| main) (|height| bubble))))
  (q> |active| ui:*gps-loader* t))

;;; accelerometer

(defconstant +const+ (/ 180 pi))

(defun move-bubble (x y z) ; called from QML
  (flet ((pitch ()
           (- (* +const+ (atan (/ y (sqrt (+ (* x x) (* z z))))))))
         (roll ()
           (- (* +const+ (atan (/ x (sqrt (+ (* y y) (* z z)))))))))
    (let* ((bubble (find-quick-item ui:*bubble*))
           (new-x (+ (|x| bubble) (roll)))
           (new-y (- (|y| bubble) (pitch))))
      ;; direct/fast function calls (will not trigger eventual animations)
      (|setX| bubble (max 0 (min new-x *max-x*)))
      (|setY| bubble (max 0 (min new-y *max-y*))))))

;;; GPS

(defun position-changed (&rest arguments)
  (q> |text| ui:*location*
      (x:join (mapcar (lambda (name value)
                        (format nil "<b>~A</b>&nbsp;&nbsp;~A" name value))
                      '("latitude" "longitude" "altitude" "h-accuracy" "v-accuracy"
                        "speed" "direction" "magnetic-variation" "timestamp")
                      arguments)
              "<br>")))

(progn
  (qml:ini-quick-view "qml/sensors.qml")
  (qlater 'ini))

