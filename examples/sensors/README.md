Basic example of efficient reading of sensor data on Sailfish, which behaves
differently from modern QML (because stuck with Qt 5.6).

The 'Compass' code is missing on purpose (see android/iOS versions, where it
works), because it doesn't work on my device, and is currently buggy on
Sailfish.

Remember to turn on 'Location' in your phone settings. The first time you run
the app, it may take a while before the first GPS values arrive.

