(qrequire :quick)

(load "lisp/message")

(qml:ini-sailfish "qml/main.qml")

(qsingle-shot 2000 (lambda ()
                     (qml:message (format nil "<h3>Message Dialog</h3>~
                                             ~%<p>A blocking message box.</p>~
                                             ~%<ul>~
                                             ~%  <li>on <b>Accept</b>, it will return the passed value, which may be any <b>Lisp</b> value~
                                             ~%  <li>on <b>Cancel</b>, it will return <b>NIL</b>~
                                             ~%</ul>"))))
