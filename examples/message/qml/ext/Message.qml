// corresponding QML to function 'qml:message'

import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
  id: message

  property string header
  property string text

  DialogHeader {
    id: header
    title: message.header
  }
  
  Text {
    x: Theme.horizontalPageMargin
    anchors.top: header.bottom
    width: parent.width - 2 * x
    text: message.text
    font.family: Theme.fontFamily
    font.pixelSize: Theme.fontSizeMedium
    color: Theme.highlightColor
    wrapMode: Text.WordWrap
  }
}
