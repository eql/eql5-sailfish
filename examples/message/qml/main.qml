import QtQuick 2.6
import Sailfish.Silica 1.0
import EQL5 1.0

ApplicationWindow {
  initialPage: Component {
    Page {
      Label {
        anchors.centerIn: parent
        text: "Hello, world!"
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeHuge
      }
    }
  }

  // corresponding function to 'qml:message'
  // needs to be defined in the app root item
  
  function message(text, header) {
    var dialog = pageStack.push(Qt.resolvedUrl("ext/Message.qml"),
                                { text: text, header: header })
    dialog.accepted.connect(function() { Lisp.call("qml:%done", true) })
    dialog.rejected.connect(function() { Lisp.call("qml:%done", false) })
  }
}
