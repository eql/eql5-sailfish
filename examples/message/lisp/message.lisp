(in-package :qml)

(let (state)
  (defun %done (result)
    (setf state result))
  (defun message (x &optional (header (tr "Info")))
    "A blocking Silica dialog, accepting any Lisp value as argument. Optionally pass a header string. When accepted, returns its argument."
    (setf state :wait)
    (qjs |message| nil (if (stringp x) x (prin1-to-string x)) header)
    (x:while (eql :wait state)
      (qsleep 0.05))
    (when state
      x)))

(export 'message)

