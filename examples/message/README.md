A blocking message dialog to be called from Lisp. Use this function instead of
`eql:qmsg`, which doesn't work correctly on Sailfish.

It returns its argument, if 'Accept' is chosen, so it can be convenient for
simple debugging (it behaves like a blocking `print`).

It can also be used as a query dialog.

Usage:
```
(qml:message (list 1 2 3))

(when (qml:message "Do you want to save the changes?" "Save?")
  (save))
```

The implementation has 3 parts to it, see:

- `lisp/message.lisp`
- `qml/ext/Message.qml`
- `qml/main.qml` (JS function `message`)

