If you want to use `(eql:qml)` for automatically generating UI vars from
`objectNane` in QML, please replace `~/.eql5/lib/qml-ui-vars.lisp` on your
Sailfish device with the file in this directory.

