## TL;DR

See **Tips** at bottom.

&nbsp;

## Introduction

*Please note that you'll need EQL5 version 21.3.4 (see `eql5 -v`) to build/run
the examples included here.*

Being Sailfish a complete Linux, development is very similar to developing on
the desktop.

Using both `ssh` and `sshfs`, and preferably connecting the device through USB
(both faster and more stable), interactive development of both Lisp and QML is
simply a pleasure.

Summary of the development approach described here:

- start Swank on the device, using the `eql5` executable (through `ssh`)
- edit files directly on the device, using either Emacs with
[qml-mode](https://github.com/coldnew/qml-mode), or QtCreator
for QML files (through `sshfs`)
- reload QML files (directly from device) with `(qml:reload)` from Slime

For creating a compiled app and the RPM for deployment, there already exist
both a nice template (for the file structure), see [eql5-sfos](https://redmine.casenave.fr/projects/eql5-sfos/repository),
and a newbie friendly description for building it all in docker, see
[Sailfish OS Builds in Docker](https://git.sr.ht/~aerique/sfosbid#codeeql5-sfoscode).

&nbsp;

## Development environment

This describes every single step for setting everything up for interactive
development.

Of course you need developer settings on the phone, and a WLAN connection for
installing the required packages.

After attaching through USB, use
```
ssh defaultuser@192.168.2.15
```
to connect to the device (if 'defaultuser' doesn't work, try 'nemo').
Since `zypper` is probably not installed yet, get it with:
```
devel-su pkcon install zypper
```
Now you can install an already prebuilt `eql5`:
```
devel-su rpm --import https://sailfish.openrepos.net/openrepos.key
devel-su zypper ar -f https://sailfish.openrepos.net/razcampagne/personal-main.repo
devel-su zypper in eql5
```
You should now be able to run both the `ecl` and `eql5` executables directly on
the device. After running `eql5` for the first time, switch to `~/.eql5/lib/`
on the device, and install `readline` and compile `ecl-readline`:
```
devel-su zypper in readline-devel
ecl --compile ecl-readline.lisp
```
For using `sshfs`, create an empty directory, e.g. `~/phone/` on the desktop
computer. To directly access the device file system from the desktop, run:
```
sshfs defaultuser@192.168.2.15: ~/phone
```
Now create your `~/phone/.eclrc` from the desktop (and on the phone) with the
following contents:
```
#+eql
(setf eql:*qtpl*            t  ; same as -qtpl
      eql:*break-on-errors* t)
```
Next time you run `eql5` on the phone (through `ssh`), both symbol completion
and line history should work.

From either `ecl` or `eql5` run:
```
(require :ecl-quicklisp)
```
and follow the instructions.

Now download and extract Slime to the phone, renaming `~/slime-<version>` to
`~/slime` (for convenience).

From desktop EQL5 `~/eql5/slime/`, copy the following 2 files to the phone:

- `eql-start-swank.lisp` to `~/slime/` on the phone
- `.swank.lisp` to `~/` on the phone, which has this contents:
```
(setf swank:*globally-redirect-io* t) ; show print output in Emacs
```
Copy some example to your phone, and start hacking after creating the ssh
tunnel:
```
ssh -L4005:127.0.0.1:4005 defaultuser@192.168.2.15
cd <path-to-example>
eql5 ~/slime/eql-start-swank.lisp run.lisp
```
After starting Emacs, do `M-x slime-connect RET RET`. Using `sshfs` from above,
you'll find the phone files mounted in e.g. `~/phone/`.
For editing QML in Emacs, you probably want [qml-mode](https://github.com/coldnew/qml-mode),
or simply use QtCreator.

After saving QML changes, reload all of QML with (from Slime):
```
(qml:reload)
```
Eventual QML errors during loading/reloading are shown in the console where you
started Swank.

The documentation of the Sailfish Silica specific QML classes can be found
online:
[sailfish-silica-all](https://sailfishos.org/develop/docs/silica/sailfish-silica-all.html/)

&nbsp;

## Building an app for deployment (RPM)

As mentioned above, there already exists a nice app template, and also a nice
description using docker for building an app, please see:

[eql5-sfos template](https://redmine.casenave.fr/projects/eql5-sfos/repository)

[Sailfish OS Builds in Docker](https://git.sr.ht/~aerique/sfosbid#codeeql5-sfoscode)

Using docker is probably the most convenient way to cross-compile and build the
final app. There also exists a fully integrated IDE (Sailfish Application SDK)
using Qt Creator plus intergrated/adapted documentation, and an emulator (in
VirtualBox).

I tried the fully integrated [Application SDK](https://sailfishos.org/wiki/Application_SDK),
but I like direct development on the device much better.

Most developers probably prefer Emacs and the command line to any IDE anyway.

For quitting `eql5` from Slime, use `(qq)`, which is short for `(eql:qquit)`.
If you forget to quit it from Slime, only a `killall -9 eql5` might help.

&nbsp;


## Notes

Since `eql:qmsg` is not correctly implemented on Sailfish, you probably want to
redefine the function to something like this:

```
(defun eql:qmsg (x)
  (print x))
```
There are some differences between official Qt QML and the Sailfish Silica one,
which uses its own classes:

- pixels are handled differently: on Sailfish, you'll need to multiply the
sizes of e.g. android QML items by a factor of 3, see also `(qml:scale)`,
which gives (for me) `3` on an android phone, `2` on an older iOS phone, and
`1` on a Sailfish phone
- some properties are not accessible as QML properties, but as Qt properties,
so to get/set e.g. the width of a Sailfish Silica `Button`, you can't use `q<`
and `q>`, instead you need `|width|` and `|setWidth|`

If you absolutely want to run an app that uses `QWidget` classes on Sailfish,
you need to set a Qt scale factor environment variable. Since it must be set
before creating the `QApplication`, you need to put this at the top of your
`main.cpp`:
```
qputenv("QT_SCALE_FACTOR", "3");
```
Be warned though: you can't (practically) use QML in a QWidget app, because it
would also be scaled by the same factor!

To stop `sshfs` (when no longer needed), do
```
/usr/bin/pkill sshfs
```
Please see also the READMEs in directories `lib/` and `utils/`.

&nbsp;


## Tips

Most convenient way of starting up everything (from desktop):

First console window:

- run `ssh -L4005:127.0.0.1:4005 defaultuser@192.168.2.15` (for Slime)
- use above shell for switching to your app source directory
- start Swank from there: `eql5 ~/slime/eql-start-swank.lisp run.lisp`

Second console window:

- run `sshfs defaultuser@192.168.2.15: ~/phone`
- switch to app source directory under `~/phone/...`
- start Emacs from there: `M-x slime-connect RET RET`
